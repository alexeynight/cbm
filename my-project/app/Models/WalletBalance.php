<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int wallet_id
 * @property float amount
 * @property string created_at
 * @property string updated_at
 */
class WalletBalance extends Model
{
    use HasFactory;
}
