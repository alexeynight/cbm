<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int id
 * @property int user_id
 * @property int currency_id
 * @property string address
 * @property int balance
 * @property string balance_last_update
 * @property string created_at
 * @property Currency currency
 * @property \Illuminate\Database\Eloquent\Collection lastBalance
 */
class Wallet extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'currency_id',
        'address',
        'balance',
        'balance_last_update',
    ];

    public $timestamps = [
        'balance_last_update',
    ];

    public function currency(): HasOne
    {
        return $this->hasOne(Currency::class, 'id', 'currency_id');
    }

    public function balances(): HasMany
    {
        return $this->hasMany(WalletBalance::class, 'wallet_id', 'id');
    }

    public function lastBalance(): HasMany
    {
        return $this->balances()->latest()->limit(1);
    }
}
