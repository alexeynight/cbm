<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string key_name
 * @property string name
 * @property int decimals
 */
class Currency extends Model
{
    use HasFactory;

    protected $fillable = [
        'key_name',
        'name',
        'decimals',
    ];
}
