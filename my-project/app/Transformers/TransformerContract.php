<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

interface TransformerContract
{
    public function transform(Model $model): array;
}
