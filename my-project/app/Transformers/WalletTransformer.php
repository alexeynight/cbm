<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;

class WalletTransformer implements TransformerContract
{
    public function transform(Model $model): array
    {
        /* @var \App\Models\Wallet $model */
        return [
            'wallet_id' => $model->id,
            'created_at' => $model->created_at,
            'balance' => round(
                ($model->balance ?? 0) / pow(10, $model->currency->decimals),
                $model->currency->decimals
            ),
            'balance_updated_at' => $model->balance_last_update,
            'currency' => $model->currency->key_name,
            'address' => $model->address,
        ];
    }
}
