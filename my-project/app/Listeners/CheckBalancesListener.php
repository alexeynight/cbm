<?php

namespace App\Listeners;

use App\Events\CheckEvent;
use App\Jobs\CheckBalanceJob;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;

class CheckBalancesListener
{
    /**
     * Handle the event.
     */
    public function handle(CheckEvent $event): void
    {
        Wallet::query()->with('currency')->chunk(
            100,
            function (Collection $wallets) {
                $wallets->each(function (Wallet $wallet) {
                    // Do balance checks asynchronously
                    dispatch(new CheckBalanceJob($wallet->id));
                });
            }
        );
    }
}
