<?php

namespace App\Http\Requests\Wallet;

use App\Models\Currency;
use App\Services\Wallet\WalletService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class WalletCreateRequest extends FormRequest
{
    public function __construct(
        private WalletService $walletService,
        array $query = [],
        array $request = [],
        array $attributes = [],
        array $cookies = [],
        array $files = [],
        array $server = [],
        $content = null
    ) {
        parent::__construct(
            $query,
            $request,
            $attributes,
            $cookies,
            $files,
            $server,
            $content
        );
    }

    public function authorize(): bool
    {
        return true;
    }

    public function rules(Request $request): array
    {
        $existsCurrencies = $this->walletService
            ->getUserCurrencies($request->user()->id)
        ;

        return [
            'address' => ['required', 'string', 'max:256'],
            'currency' => ['required',
                'string',
                Rule::notIn($existsCurrencies->toArray()),
                'exists:'.Currency::class.',key_name',
            ],
        ];
    }
}
