<?php

namespace App\Http\Controllers;

use App\DTO\Wallets\CreateWalletDTO;
use App\Http\Requests\Wallet\WalletCreateRequest;
use App\Models\Wallet;
use App\Services\Wallet\WalletService;
use App\Transformers\WalletTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WalletController extends Controller
{
    public function create(
        WalletCreateRequest $request,
        WalletService $walletService
    ): Response {
        $createWalletDTO = new CreateWalletDTO(
            $request->user()->id,
            $request->get('currency'),
            $request->get('address')
        );

        $wallet = $walletService->createWallet($createWalletDTO);

        return response(
            (new WalletTransformer())->transform($wallet),
            Response::HTTP_CREATED
        );
    }

    public function list(
        Request $request,
        WalletService $walletService
    ): Response {
        $wallets = $walletService->getUserWallets($request->user()->id);

        return response(
            $wallets->map(function (Wallet $wallet) {
                return (new WalletTransformer())->transform($wallet);
            })->toArray(),
            Response::HTTP_OK
        );
    }

    public function get(
        Request $request,
        WalletService $walletService
    ): Response {
        $wallet = $walletService->getOwnerWallet(
            $request->route('id'),
            $request->user()->id
        );

        return response(
            (new WalletTransformer())->transform($wallet),
            Response::HTTP_OK
        );
    }
}
