<?php

namespace App\Http\Middleware;

use App\Services\Wallet\WalletService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class WalletOwner
{
    public function __construct(private WalletService $walletService)
    {
    }

    public function handle(Request $request, \Closure $next)
    {
        if (!$this->walletService->isOwner(
            $request->route('id'),
            $request->user()->id
        )) {
            throw new ModelNotFoundException('Not found');
        }

        return $next($request);
    }
}
