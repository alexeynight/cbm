<?php

namespace App\DTO\Wallets;

class CreateWalletDTO
{
    public function __construct(
        private int $userId,
        private string $currency,
        private string $address
    ) {
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}
