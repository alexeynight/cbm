<?php

namespace App\DTO\Wallets;

class WalletDTO
{
    public function __construct(
        private int $walletId,
        private int $balance,
        private string $lastOperation,
    ) {
    }

    public function getWalletId(): int
    {
        return $this->walletId;
    }

    public function getBalance(): int
    {
        return $this->balance;
    }

    public function getLastOperation(): string
    {
        return $this->lastOperation;
    }
}
