<?php

namespace App\Services\Balance;

use App\DTO\Wallets\WalletDTO;
use App\Repositories\BalanceRepository;

class BalanceService
{
    public function __construct(
        private BalanceRepository $balanceRepository
    ) {
    }

    public function saveBalanceHistory(WalletDTO $walletDTO)
    {
        $this->balanceRepository->insertBalance(
            $walletDTO->getWalletId(),
            $walletDTO->getBalance()
        );

        $this->balanceRepository->updateWalletBalance(
            $walletDTO->getWalletId(),
            $walletDTO->getBalance(),
            $walletDTO->getLastOperation()
        );
    }
}
