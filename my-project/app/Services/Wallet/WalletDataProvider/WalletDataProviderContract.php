<?php

namespace App\Services\Wallet\WalletDataProvider;

use App\DTO\Wallets\WalletDTO;
use App\Models\Wallet;

interface WalletDataProviderContract
{
    public function getBalance(Wallet $wallet): WalletDTO;
}
