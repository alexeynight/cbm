<?php

namespace App\Services\Wallet\WalletDataProvider;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Collection;

abstract class AbstractWalletDataProvider implements WalletDataProviderContract
{
    public function __construct(
        protected PendingRequest $httpClient,
        protected Collection $config
    ) {
    }
}
