<?php

namespace App\Services\Wallet\WalletDataProvider;

use App\DTO\Wallets\WalletDTO;
use App\Exceptions\WalletDataReceivingError;
use App\Models\Wallet;
use Carbon\Carbon;

class EtherscanWalletDataProvider extends AbstractWalletDataProvider
{
    public const OPERATION_BALANCE = 'balance';
    public const OPERATION_TRANSACTION = 'txlist';

    private const RESPONSE_OK_STATUS = '1';
    private const RESPONSE_OK_MESSAGE = 'OK';

    public function getBalance(Wallet $wallet): WalletDTO
    {
        try {
            $transactionData = $this->lastTransaction($wallet);
            $balanceData = $this->accountBalance($wallet);

            return new WalletDTO(
                $wallet->id,
                (int) $balanceData,
                Carbon::createFromTimestamp($transactionData)->toDateTimeString()
            );
        } catch (\Throwable $exception) {
            throw new WalletDataReceivingError('Can\'t receive Data for Wallet');
        }
    }

    private function accountBalance(Wallet $wallet)
    {
        $balance = $this->getRequest([
            'module' => 'account',
            'tag' => 'latest',
            'action' => self::OPERATION_BALANCE,
            'address' => $wallet->address,
        ]);

        return $balance['result'];
    }

    private function lastTransaction(Wallet $wallet)
    {
        $transaction = $this->getRequest([
            'module' => 'account',
            'action' => self::OPERATION_TRANSACTION,
            'address' => $wallet->address,
            'page' => 1,
            'offset' => 1,
            'sort' => 'desc',
        ]);

        return $transaction['result'][0]['timeStamp'];
    }

    /**
     * @throws \Exception
     */
    private function getRequest(
        array $parameters
    ): array {
        $response = $this->httpClient->get(
            $this->config->get('base_url'),
            array_merge($parameters, [
                'apikey' => $this->config->get('api_key'),
            ])
        )->json();

        if (self::RESPONSE_OK_STATUS !== $response['status'] || self::RESPONSE_OK_MESSAGE !== $response['message']) {
            dd($response, __FILE__, __LINE__);
            throw new \RuntimeException();
        }

        return $response;
    }
}
