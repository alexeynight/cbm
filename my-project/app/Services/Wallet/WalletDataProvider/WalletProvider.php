<?php

namespace App\Services\Wallet\WalletDataProvider;

use App\DTO\Wallets\WalletDTO;
use App\Exceptions\UndefinedWalletDataProvider;
use App\Models\Wallet;
use Illuminate\Support\Collection;

final class WalletProvider
{
    private WalletDataProviderContract $provider;

    public function __construct(
        private Collection $providers
    ) {
    }

    public function setProvider(string $currencyKey): WalletProvider
    {
        $this->provider = $this->providers->get($currencyKey);

        if (!$this->provider) {
            throw new UndefinedWalletDataProvider('Unknown currency');
        }

        return $this;
    }

    public function getBalance(Wallet $wallet): WalletDTO
    {
        return $this->provider->getBalance($wallet);
    }
}
