<?php

namespace App\Services\Wallet\WalletDataProvider;

use App\DTO\Wallets\WalletDTO;
use App\Exceptions\WalletDataReceivingError;
use App\Models\Wallet;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;

class BlockchairWalletDataProvider extends AbstractWalletDataProvider
{
    public const OPERATION_BALANCE = 'balance';

    public function getBalance(Wallet $wallet): WalletDTO
    {
        try {
            $response = $this->getRequest(
                $wallet->currency->key_name,
                self::OPERATION_BALANCE,
                ['address' => $wallet->address]
            );

            $data = $response->json()['data'][$wallet->address]['address'];

            return new WalletDTO(
                $wallet->id,
                $data['balance'],
                max($data['last_seen_receiving'], $data['last_seen_spending'])
            );
        } catch (\Throwable $exception) {
            throw new WalletDataReceivingError('Can\'t receive Data for Wallet');
        }
    }

    /**
     * @throws \Exception
     */
    private function getRequest(
        string $currency,
        string $operation,
        array $urlParameters
    ): PromiseInterface|Response {
        $url = $this->config->get('currencies')[$currency].
            $this->config->get('api_urls')[$operation];

        return $this->httpClient->withUrlParameters($urlParameters)->send(
            'get',
            $url
        );
    }
}
