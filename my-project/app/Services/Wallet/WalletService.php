<?php

namespace App\Services\Wallet;

use App\DTO\Wallets\CreateWalletDTO;
use App\Models\Wallet;
use App\Repositories\CurrencyRepository;
use App\Repositories\WalletRepository;
use Illuminate\Support\Collection;

class WalletService
{
    public function __construct(
        private CurrencyRepository $currencyRepository,
        private WalletRepository $walletRepository
    ) {
    }

    public function createWallet(CreateWalletDTO $walletDTO): Wallet
    {
        $currency = $this->currencyRepository
            ->getCurrencyByKey($walletDTO->getCurrency())
        ;

        return $this->walletRepository->saveWallet(
            $walletDTO->getUserId(),
            $currency->id,
            $walletDTO->getAddress()
        );
    }

    public function getUserWallets(int $userId): Collection
    {
        return $this->walletRepository->getUserWallets($userId);
    }

    public function getUserCurrencies(int $userId): Collection
    {
        return $this->walletRepository
            ->getUserWallets($userId)
            ->pluck('currency')
            ->pluck('key_name')
        ;
    }

    public function getOwnerWallet(int $walletId, int $userId): ?Wallet
    {
        return $this->walletRepository->getWallet($walletId, $userId);
    }

    public function getWallet(int $walletId): ?Wallet
    {
        return $this->walletRepository->getWallet($walletId);
    }

    public function isOwner(int $walletId, int $userId): bool
    {
        return $this->walletRepository->existsWallet($walletId, $userId);
    }
}
