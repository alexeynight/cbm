<?php

namespace App\Services\Wallet;

use App\DTO\Wallets\WalletDTO;
use App\Models\Wallet;
use App\Services\Wallet\WalletDataProvider\WalletProvider;

class WalletDataService
{
    public function __construct(
        private WalletProvider $walletProvider
    ) {
    }

    public function getBalance(Wallet $wallet): WalletDTO
    {
        return $this->walletProvider
            ->setProvider($wallet->currency->key_name)
            ->getBalance($wallet)
        ;
    }

    /**
     * Maybe we need it in feature.
     */
    public function getTransactions()
    {
    }
}
