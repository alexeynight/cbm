<?php

namespace App\Providers;

use App\Services\Wallet\WalletDataProvider\WalletProvider;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;

class WalletServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        (new Collection(config('wallets.providers')))->each(function ($config, $className) {
            $this->app->singleton($className, function () use ($config, $className) {
                $client = Http::withOptions([
                    'base_uri' => $config['base_url'],
                    'timeout' => $config['timeout'],
                    'connect_timeout' => $config['connect_timeout'],
                ]);

                return new $className($client, new Collection($config));
            });
        });

        $this->app->singleton(WalletProvider::class, function () {
            $strategies = new Collection();

            (new Collection(
                config('wallets.sources'))
            )->each(function ($className, $currencyKey) use ($strategies) {
                $strategies->put(
                    $currencyKey,
                    $this->app->get($className)
                );
            });

            return new WalletProvider($strategies);
        });
    }

    public function boot(): void
    {
    }
}
