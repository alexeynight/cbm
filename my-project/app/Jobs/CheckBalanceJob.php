<?php

namespace App\Jobs;

use App\Services\Balance\BalanceService;
use App\Services\Wallet\WalletDataService;
use App\Services\Wallet\WalletService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckBalanceJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        private int $walletId
    ) {
    }

    /**
     * Execute the job.
     */
    public function handle(
        WalletDataService $walletDataService,
        BalanceService $balanceService,
        WalletService $walletService
    ): void {
        $wallet = $walletService->getWallet($this->walletId);

        $balanceDTO = $walletDataService->getBalance($wallet);
        $balanceService->saveBalanceHistory($balanceDTO);
    }
}
