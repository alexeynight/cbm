<?php

namespace App\Repositories;

use App\Models\Wallet;
use App\Models\WalletBalance;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BalanceRepository
{
    public function insertBalance(int $walletId, int $amount)
    {
        $now = Carbon::now();

        WalletBalance::query()->insert([
            'wallet_id' => $walletId,
            'amount' => $amount,
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }

    public function updateWalletBalance(int $walletId, int $balance, string $lastUpdate)
    {
        DB::transaction(function () use ($walletId, $balance, $lastUpdate) {
            /** @var Wallet $wallet */
            $wallet = Wallet::query()->lockForUpdate()->find($walletId);
            if ($lastUpdate <= $wallet->balance_last_update) {
                return;
            }
            $wallet->update([
                'balance' => $balance,
                'balance_last_update' => Carbon::parse($lastUpdate),
            ]);
        });
    }
}
