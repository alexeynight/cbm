<?php

namespace App\Repositories;

use App\Exceptions\WalletCreatingError;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;

class WalletRepository
{
    public function saveWallet(
        int $userid,
        int $currencyId,
        string $address
    ): Wallet {
        $wallet = (new Wallet([
            'user_id' => $userid,
            'currency_id' => $currencyId,
            'address' => $address,
        ]));
        if (!$wallet->save()) {
            throw new WalletCreatingError('Wallet hasn\'t been created');
        }

        return $wallet;
    }

    public function getUserWallets(int $userId): Collection
    {
        return Wallet::query()
            ->with('currency')
            ->where('user_id', $userId)
            ->get()
        ;
    }

    public function getWallet(int $walletId, ?int $userId = null): ?Wallet
    {
        $query = Wallet::query()
            ->with('currency')
            ->where('id', $walletId)
        ;
        if ($userId) {
            $query->where('user_id', $userId);
        }

        return $query->first();
    }

    public function existsWallet(int $walletId, int $userId): bool
    {
        return Wallet::query()
            ->with('currency')
            ->where('user_id', $userId)
            ->where('id', $walletId)
            ->exists()
        ;
    }
}
