<?php

namespace App\Repositories;

use App\Models\Currency;

class CurrencyRepository
{
    public function getCurrencyByKey(string $keyName): ?Currency
    {
        return Currency::query()
            ->where('key_name', strtoupper($keyName))
            ->first()
        ;
    }
}
