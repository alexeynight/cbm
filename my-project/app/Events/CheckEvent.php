<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CheckEvent
{
    use Dispatchable;
    use SerializesModels;

    /**
     * Domain Event of Checking Start.
     */
    public function __construct()
    {
    }
}
