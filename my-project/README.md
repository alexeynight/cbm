
## Add wallet

```
curl -XPOST -H "Content-type: application/json"  -H 'Authorization: Bearer token' -d '{"currency":"LTC", "address":"ltc1qzvcgmntglcuv4smv3lzj6k8szcvsrmvk0phrr9wfq8w493r096ssm2fgsw"}' 'http://localhost:8000/api/wallets'                                
```

## Get all wallets by user

```
curl -XGET -H "Content-type: application/json"  -H 'Authorization: Bearer token' 'http://localhost:8000/api/wallets'                                
```

## Get specific (by wallet_id) wallet by user

```
curl -XGET -H "Content-type: application/json"  -H 'Authorization: Bearer token' 'http://localhost:8000/api/wallet/:id'                                
```

## Additionals

For creating requests you need Bearer token.

After container started you should run command:
```
php artisan db:seed
```
