<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();
            $table->string('key_name', 3)->index();
            $table->string('name', 30);
            $table->integer('decimals');
            $table->timestamps();
        });

        Schema::create('wallets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('currency_id');
            $table->string('address', 256);
            $table->unsignedBigInteger('balance')->nullable();
            $table->timestamp('balance_last_update')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
            ;

            $table->foreign('currency_id')
                ->references('id')
                ->on('currencies')
            ;
        });

        Schema::create('wallet_balances', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('wallet_id')->index();
            $table->unsignedBigInteger('amount');
            $table->timestamps();

            $table->index(['wallet_id', 'created_at']);

            $table->foreign('wallet_id')
                ->references('id')
                ->on('wallets')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('wallets');
        Schema::dropIfExists('currencies');
        Schema::dropIfExists('wallet_balances');
    }
};
