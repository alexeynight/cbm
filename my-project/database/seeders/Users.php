<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::query()->create([
            'name' => 'James Bond',
            'email' => 'james@bond.com',
            'password' => bcrypt('12345'),
        ]);

        User::query()->create([
            'name' => 'Bill Gates',
            'email' => 'bill@micro.com',
            'password' => bcrypt('12345'),
        ]);
    }
}
