<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class Currencies extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Currency::query()->create([
            'key_name' => 'BTC',
            'name' => 'Bitcoin',
            'decimals' => 8,
        ]);

        Currency::query()->create([
            'key_name' => 'LTC',
            'name' => 'Litecoin',
            'decimals' => 8,
        ]);

        Currency::query()->create([
            'key_name' => 'ETH',
            'name' => 'Ethereum',
            'decimals' => 18,
        ]);
    }
}
