<?php

use App\Services\Wallet\WalletDataProvider\BlockchairWalletDataProvider;

return [
    'sources' => [
        'BTC' => App\Services\Wallet\WalletDataProvider\BlockchairWalletDataProvider::class,
        'LTC' => App\Services\Wallet\WalletDataProvider\BlockchairWalletDataProvider::class,
        'ETH' => App\Services\Wallet\WalletDataProvider\EtherscanWalletDataProvider::class,
    ],

    'providers' => [
        App\Services\Wallet\WalletDataProvider\BlockchairWalletDataProvider::class => [
            'api_key' => env('BLOCKCHAIR_API_KEY'),
            'base_url' => 'https://api.blockchair.com/',
            'timeout' => 10,
            'connect_timeout' => 3,
            'api_urls' => [
                BlockchairWalletDataProvider::OPERATION_BALANCE => 'dashboards/address/{address}',
            ],
            'currencies' => [
                'BTC' => 'bitcoin/',
                'LTC' => 'litecoin/',
            ],
        ],
        App\Services\Wallet\WalletDataProvider\EtherscanWalletDataProvider::class => [
            'api_key' => env('ETHERSCAN_API_KEY'),
            'base_url' => 'https://api.etherscan.io/api',
            'timeout' => 10,
            'connect_timeout' => 3,
        ],
    ],
];
