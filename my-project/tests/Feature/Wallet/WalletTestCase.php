<?php

namespace Tests\Feature\Wallet;

use App\DTO\Wallets\WalletDTO;
use App\Models\Currency;
use App\Models\Wallet;
use App\Services\Wallet\WalletDataService;
use Carbon\Carbon;

/**
 * @internal
 *
 * @coversNothing
 */
class WalletTestCase extends \Tests\TestCase
{
    private WalletDataService $walletDataService;

    public function setUp(): void
    {
        $this->walletDataService = app(WalletDataService::class);
    }

    public function it_should_be_able_retrieve_btc_data()
    {
        $currency = Currency::query()
                            ->where('key_name', '=', 'BTC')
                            ->first();

        $wallet = new Wallet([
            'user_id' => 1,
            'currency' => $currency->id,
            'address' => '34xp4vRoCGJym3xR7yCVPFHoCNxv4Twseo',
        ]);

        $walletDTO = $this->walletDataService->getBalance($wallet);

        $this->assertTrue($walletDTO->getBalance() >= 0);
        $this->assertTrue($walletDTO->getLastOperation() >= Carbon::parse('1970-01-01'));
    }
}
